import numpy as np
import random
import tensorflow as tf
from load_data import DataGenerator, DataGeneratorFast
from tensorflow.python.platform import flags
from tensorflow.keras import layers
import time

FLAGS = flags.FLAGS

flags.DEFINE_integer(
    'num_classes', 5, 'number of classes used in classification (e.g. 5-way classification).')

flags.DEFINE_integer('num_samples', 1,
                     'number of examples used for inner gradient update (K for K-shot learning).')

flags.DEFINE_integer('meta_batch_size', 16,
                     'Number of N-way classification tasks per batch')

flags.DEFINE_integer('hidden_size', 128,
                     'size of lstm hidden layer')

def loss_function(preds, labels):
    """
    Computes MANN loss
    Args:
        preds: [B, K+1, N, N] network output
        labels: [B, K+1, N, N] labels
    Returns:
        scalar loss
    """
    cce = tf.keras.losses.CategoricalCrossentropy(from_logits=True)
    loss = cce(labels[:, -1], preds[:, -1])

    return loss
    

class MANN(tf.keras.Model):

    def __init__(self, num_classes, samples_per_class):
        super(MANN, self).__init__()
        self.num_classes = num_classes
        self.samples_per_class = samples_per_class
        self.layer1 = tf.keras.layers.LSTM(FLAGS.hidden_size, return_sequences=True)
        self.layer2 = tf.keras.layers.LSTM(num_classes, return_sequences=True)

    def call(self, input_images, input_labels):
        """
        MANN
        Args:
            input_images: [B, K+1, N, 784] flattened images
            labels: [B, K+1, N, N] ground truth labels
        Returns:
            [B, K+1, N, N] predictions
        """
        B, Kp1, N, IMAGE_SIZE = input_images.shape
        inputs_sequence = tf.reshape(input_images, [-1, Kp1 * N, IMAGE_SIZE])
        zero_last = tf.zeros_like(labels[:, -1:])
        labels_zero_last = tf.concat([labels[:, :-1], zero_last], axis=1)
        labels_sequence = tf.reshape(labels_zero_last, [-1, Kp1 * N, N]) 
        layer1_input = tf.concat([inputs_sequence, labels_sequence], axis=-1)
        out1 = self.layer1(layer1_input)
        out2 = self.layer2(out1)
        out = tf.reshape(out2, [-1, Kp1, N, N])
        return out

ims = tf.compat.v1.placeholder(tf.float32, shape=(
    None, FLAGS.num_samples + 1, FLAGS.num_classes, 784))
labels = tf.compat.v1.placeholder(tf.float32, shape=(
    None, FLAGS.num_samples + 1, FLAGS.num_classes, FLAGS.num_classes))

data_generator = DataGeneratorFast(
    FLAGS.num_classes, FLAGS.num_samples + 1)

o = MANN(FLAGS.num_classes, FLAGS.num_samples + 1)
out = o(ims, labels)

loss = loss_function(out, labels)
optim = tf.compat.v1.train.AdamOptimizer(3e-4)
optimizer_step = optim.minimize(loss)

# Empty log file.
with open(f"log_{FLAGS.num_classes}_{FLAGS.num_samples}_{FLAGS.meta_batch_size}_{FLAGS.hidden_size}.csv", "w") as f:
    f.write("step, accuracy\n")


gpu_options = tf.compat.v1.GPUOptions(per_process_gpu_memory_fraction=0.2)
with tf.compat.v1.Session(config=tf.compat.v1.ConfigProto(gpu_options=gpu_options)) as sess:
    sess.run(tf.compat.v1.local_variables_initializer())
    sess.run(tf.compat.v1.global_variables_initializer())

    for step in range(50000):
        start_time = time.time()
        i, l = data_generator.sample_batch('train', FLAGS.meta_batch_size)
        feed = {ims: i.astype(np.float32), labels: l.astype(np.float32)}
        end_time = time.time()
        #print(f"batch time {end_time - start_time}")
        start_time = time.time()
        _, ls = sess.run([optimizer_step, loss], feed)
        end_time = time.time()
        #print(f"train time {end_time - start_time}")

        if step % 100 == 0:
            print("*" * 5 + "Iter " + str(step) + "*" * 5)
            i, l = data_generator.sample_batch('test', 100)
            feed = {ims: i.astype(np.float32),
                    labels: l.astype(np.float32)}
            pred, tls = sess.run([out, loss], feed)
            print("Train Loss:", ls, "Test Loss:", tls)
            pred = pred.reshape(
                -1, FLAGS.num_samples + 1,
                FLAGS.num_classes, FLAGS.num_classes)
            pred = pred[:, -1, :, :].argmax(2)
            l = l[:, -1, :, :].argmax(2)
            accuracy = (1.0 * (pred == l)).mean()
            print("Test Accuracy", accuracy)
            with open(f"log_{FLAGS.num_classes}_{FLAGS.num_samples}_{FLAGS.meta_batch_size}_{FLAGS.hidden_size}.csv", "a") as f:
                f.write(f"{step}, {accuracy}\n")
