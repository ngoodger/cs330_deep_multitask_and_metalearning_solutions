import numpy as np
import os
import random
import tensorflow as tf
import imageio


def get_images(paths, labels, nb_samples=None, shuffle=True):
    """
    Takes a set of character folders and labels and returns paths to image files
    paired with labels.
    Args:
        paths: A list of character folders
        labels: List or numpy array of same length as paths
        nb_samples: Number of images to retrieve per character
    Returns:
        List of (label, image_path) tuples
    """
    if nb_samples is not None:
        sampler = lambda x: random.sample(x, nb_samples)
    else:
        sampler = lambda x: x
    images_labels = [(i, os.path.join(path, image))
                     for i, path in zip(labels, paths)
                     for image in sampler(os.listdir(path))]
    if shuffle:
        random.shuffle(images_labels)
    return images_labels


def image_file_to_array(filename, dim_input):
    """
    Takes an image path and returns numpy array
    Args:
        filename: Image filename
        dim_input: Flattened shape of image
    Returns:
        1 channel image
    """
    image = imageio.imread(filename)
    image = image.reshape([dim_input])
    image = image.astype(np.float32) / 255.0
    image = 1.0 - image
    return image

class DataGeneratorFast(object):
    """
    Data Generator capable of generating batches of Omniglot data.
    A "class" is considered a class of omniglot digits.
    """

    def __init__(self, num_classes, num_samples_per_class, config={}):
        """
        Args:
            num_classes: Number of classes for classification (K-way)
            num_samples_per_class: num samples to generate per class in one batch
            batch_size: size of meta batch size (e.g. number of functions)
        """
        self.num_samples_per_class = num_samples_per_class
        self.num_classes = num_classes

        data_folder = config.get('data_folder', '../omniglot_resized')
        self.img_size = config.get('img_size', (28, 28))

        self.dim_input = np.prod(self.img_size)
        self.dim_output = self.num_classes

        character_folders = [os.path.join(data_folder, family, character)
                             for family in os.listdir(data_folder)
                             if os.path.isdir(os.path.join(data_folder, family))
                             for character in os.listdir(os.path.join(data_folder, family))
                             if os.path.isdir(os.path.join(data_folder, family, character))]

        random.seed(2)
        np.random.seed(2)
        random.shuffle(character_folders)
        num_val = 100
        num_train = 1100
        self.metatrain_character_folders = character_folders[: num_train]
        self.metaval_character_folders = character_folders[
            num_train:num_train + num_val]
        self.metatest_character_folders = character_folders[
            num_train + num_val:]

        self.metatrain_sample_count = len(self.metatrain_character_folders)
        self.metaval_sample_count = len(self.metaval_character_folders)
        self.metatest_sample_count = len(self.metatest_character_folders)

        self.metatrain_character_label_images = get_images(self.metatrain_character_folders, range(self.metatrain_sample_count), 20, shuffle=False)
        self.metaval_character_label_images = get_images(self.metaval_character_folders, range(self.metaval_sample_count), 20, shuffle=False)
        self.metatest_character_label_images = get_images(self.metatest_character_folders, range(self.metatest_sample_count), 20, shuffle=False)
        
        def read_labels_images(dataset):
            labels, images_files = zip(*(dataset))
            images = np.stack([image_file_to_array(image_file, 784) for image_file in images_files])
            return labels, images


        self.metatrain_character_dataset = read_labels_images(self.metatrain_character_label_images)
        self.metaval_character_dataset = read_labels_images(self.metaval_character_label_images)
        self.metatest_character_dataset = read_labels_images(self.metatest_character_label_images)
        labels_one_hot_class = np.eye(self.num_classes)
        self.base_labels = np.swapaxes(np.repeat(labels_one_hot_class, self.num_samples_per_class, axis=1), 0, 1)
        sample_idxs = list(range(self.num_classes * self.num_samples_per_class))
        out_idxs = [sample_idxs.pop(i * self.num_samples_per_class - i) for i in range(self.num_classes)]
        self.base_labels = np.concatenate((self.base_labels[sample_idxs], self.base_labels[out_idxs]))

        

    def sample_batch(self, batch_type, batch_size):
        """
        Samples a batch for training, validation, or testing
        Args:
            batch_type: train/val/test
        Returns:
            A a tuple of (1) Image batch and (2) Label batch where
            image batch has shape [B, K, N, 784] and label batch has shape [B, K, N, N]
            where B is batch size, K is number of samples per class, N is number of classes
        """
        if batch_type == "train":
            labels, images = self.metatrain_character_dataset
            sample_count = self.metatrain_sample_count
        elif batch_type == "val":
            labels, images = self.metaval_character_dataset
            sample_count = self.metaval_sample_count
        else:
            labels, images = self.metatest_character_dataset
            sample_count = self.metatest_sample_count

        images_list, labels_list = [], []
        for batch_idx in range(batch_size):
            class_idxs = np.expand_dims(np.random.randint(sample_count, size=self.num_classes), axis=1)
            intraclass_idxs = np.random.randint(20, size=self.num_classes * self.num_samples_per_class).reshape((self.num_classes, self.num_samples_per_class))
            sample_idxs = (class_idxs * 20 + intraclass_idxs).flatten().tolist()
            out_idxs = [sample_idxs.pop(i * self.num_samples_per_class - i) for i in range(self.num_classes)]
            sampled_images = np.concatenate((images[sample_idxs], images[out_idxs]))
            random_input_idx = np.arange(len(sample_idxs))
            np.random.shuffle(random_input_idx)
            random_output_idx = np.arange(len(sample_idxs), len(sample_idxs) + self.num_classes)
            np.random.shuffle(random_output_idx)
            random_idx = np.concatenate((random_input_idx, random_output_idx))
            random_sample_images = sampled_images[random_idx].reshape([self.num_samples_per_class, self.num_classes, 784])
            random_sample_labels = self.base_labels[random_idx].reshape([self.num_samples_per_class, self.num_classes, self.num_classes])

            images_list.append(random_sample_images)
            labels_list.append(random_sample_labels)

        all_image_batches = np.stack(images_list) 
        all_label_batches = np.stack(labels_list) 

        return all_image_batches, all_label_batches



class DataGenerator(object):
    """
    Data Generator capable of generating batches of Omniglot data.
    A "class" is considered a class of omniglot digits.
    """

    def __init__(self, num_classes, num_samples_per_class, config={}):
        """
        Args:
            num_classes: Number of classes for classification (K-way)
            num_samples_per_class: num samples to generate per class in one batch
            batch_size: size of meta batch size (e.g. number of functions)
        """
        self.num_samples_per_class = num_samples_per_class
        self.num_classes = num_classes

        data_folder = config.get('data_folder', '../omniglot_resized')
        self.img_size = config.get('img_size', (28, 28))

        self.dim_input = np.prod(self.img_size)
        self.dim_output = self.num_classes

        character_folders = [os.path.join(data_folder, family, character)
                             for family in os.listdir(data_folder)
                             if os.path.isdir(os.path.join(data_folder, family))
                             for character in os.listdir(os.path.join(data_folder, family))
                             if os.path.isdir(os.path.join(data_folder, family, character))]

        random.seed(2)
        np.random.seed(2)
        random.shuffle(character_folders)
        num_val = 100
        num_train = 1100
        self.metatrain_character_folders = character_folders[: num_train]
        self.metaval_character_folders = character_folders[
            num_train:num_train + num_val]
        self.metatest_character_folders = character_folders[
            num_train + num_val:]

    def one_hot(self, index):
        one_hot_value = np.zeros(self.num_classes)
        one_hot_value[index] = 1
        return one_hot_value


    def sample_batch(self, batch_type, batch_size):
        """
        Samples a batch for training, validation, or testing
        Args:
            batch_type: train/val/test
        Returns:
            A a tuple of (1) Image batch and (2) Label batch where
            image batch has shape [B, K, N, 784] and label batch has shape [B, K, N, N]
            where B is batch size, K is number of samples per class, N is number of classes
        """
        if batch_type == "train":
            folders = self.metatrain_character_folders
        elif batch_type == "val":
            folders = self.metaval_character_folders
        else:
            folders = self.metatest_character_folders

        images_list, labels_list = [], []
        for batch_idx in range(batch_size):
            
            data = get_images(random.sample(folders, self.num_classes), range(self.num_classes), self.num_samples_per_class, False)
            # Get first example of each class.
            out_data = [data.pop(i * self.num_samples_per_class - i) for i in range(self.num_classes)]
            random.shuffle(data)
            random.shuffle(out_data)
            labels, images  = zip(*(data + out_data))
            image_np = np.vstack([image_file_to_array(image, 784) for image in images]).reshape([self.num_samples_per_class, self.num_classes, 784])
            label_np = np.vstack([self.one_hot(label) for label in labels]).reshape([self.num_samples_per_class, self.num_classes, self.num_classes])
            label_np_test = np.vstack([self.one_hot(label) for label in labels])
            images_list.append(image_np)
            labels_list.append(label_np)

        all_image_batches = np.stack(images_list) 
        all_label_batches = np.stack(labels_list) 

        return all_image_batches, all_label_batches
