See plots.ipynb for plots of the results.

# Problem 1
Implemented both a slow and fast implementation of the dataloader.

The fast preloads the images to memory and allows for training with larger batch sizes.

Further improvement could be made if the batch dimension is also built using vectorised operations.

# Problem 2

Implemented in tensorflow.  Reduced learning rate to 3e-4 and added logging for plotting of results.

# Problem 3

In order to use a learning rate of 3e-4 a batch size of 256 is required for the expected configurations or the model initially learns to produce fixed output and 1/N\_CLASSES accuracy. This is what prompted the fast implementation of the dataloader as without it training would take a very long time.

As expected the final accuracy of the model goes down with the number of classes and up with the number of samples per class.

# Problem 4a

I made the hidden size a parameter and set it to 1024 to see if I could do better on the 1-shot 4 class learning case.  The inspiration for this came from the recent success of GPT-3 and the possibility that larger models might be much better at metalearning.  This comparison is probably naive but at least the intuition turned out to be correct with the larger model reaching approximately 90% accuracy while the 128 hidden size version only reached approximately 60% accuracy with the same hyperparameters.
